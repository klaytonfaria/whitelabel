module.exports = function(grunt) {
	var returnObj = {};	

	grunt.file.recurse("src/template_data/", function(abspath) {
		var ext = abspath.split(".");
		ext = ext[ext.length-1];

		if(ext === "json") {			
			var data = grunt.file.readJSON(abspath);
			if(data) {				
				returnObj[data.partnerSlug + "_" + data.fileType] = {
					templateData: abspath,
					template: "src/template/" + data.template,
					output: "build/" + data.fileType + "/" + data.partnerSlug + "." + data.fileType
				}
			}
		}
		
	});
	
	return returnObj;
};