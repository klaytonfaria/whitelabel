module.exports = {
    dist: {
      options: {
        sassDir: 'build/scss/',
        cssDir: 'dist/css/',        
        force: true
      }
    }
};
