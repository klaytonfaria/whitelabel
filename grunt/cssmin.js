module.exports = {
  	dist: {
  		options: {
  			sourceMap: true
  		},
	    files: [
	   		{
			    expand: true,
			    cwd: "dist/css",
			    src: ["*.css"],
			    dest: "dist/css"
	    	}
	    ]
	}    
};
