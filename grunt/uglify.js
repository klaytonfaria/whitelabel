module.exports = {
    dist: {
    	options: {
	      sourceMap: true
	    },
        files: [	   		
	    	{
			    expand: true,
			    cwd: "build/js",			    
			    src: ["*.js"],
			    dest: "dist/js"
	    	}
	    ]
    }
};
