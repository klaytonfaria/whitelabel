# Whitelabel static generator

----

> Generate Whitelabel static assets rendering handlebars templates.

----
## Dependencies
1. node (npm Package manager)
2. grunt-cli.

----
    npm install -g grunt-cli

### Install grunt dependencies
    npm install

----
## Grunt tasks

#### Build assets
    grunt build
output folder: *dist/*

#### Clear assets
    grunt clear
#### Watch data 
*(When json has changed build task will be started)*

    grunt 

